import Vue from 'vue'
import Router from 'vue-router'
import Auth from '@/components/Auth'
import Login from '@/components/Login'
import Signup from '@/components/Signup'
import Archive from '@/components/Archive'
import ArchiveView from '@/components/ArchiveView'
import Dashboard from '@/components/Dashboard'
import Mail from '@/components/Mail'
import Lecture from '@/components/Lecture'

import store from '../store/index'
import { CHECK_AUTH } from '../store/actions.type';

Vue.use(Router)

export default new Router({
  // mode: 'history',
  // hashbang: false,
  routes: [
    {
      path: '/',
      component: Auth,
      children: [{
          name: 'login',
          path: '',
          component: Login
      }, {
          name: 'registration',
          path: '/signup',
          component: Signup
      }],
      beforeEnter (to, from, next) {
        store.dispatch(CHECK_AUTH)
        .then((loggedIn) => loggedIn ? next({name: 'archive'}) : next())
        .catch(() => next())
      }
    },
    {
      path: '/app',
      component: Dashboard,
      meta: { requiresAuth: true },
      children: [{
        path: '/app/archive',
        name: 'archive',
        component: Archive
      }, {
        path: '/app/mail',
        name: 'mail',
        component: Mail
      }, {
        path: '/app/lecture',
        name: 'lecture',
        component: Lecture
      },
      {
        path: '/app/archive/:category/:id',
        name: 'post',
        component: ArchiveView
      }],
      // What's better, this beforeEnter or beforeEach? (Propably question to Tsopa)
      // beforeEnter (to, from, next) {
      //   store.dispatch(CHECK_AUTH)
      //   .then((loggedIn) => loggedIn ? next() : next('/'))
      //   .catch(() => next(false))
      // }
    }
  ]
})
