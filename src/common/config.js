export default {}
// export const API_URL = 'http://clms.ga'
export const API_URL = 'http://localhost:6534'

/* VK OAuth parameters */
export const VK_CLIENT_ID = '6429925l'
export const VK_REDIRECT_URI = 'http://clms.ga/vkAuth'
export const VK_SCOPE = 'notify,email'
export const VK_DISPLAY = 'page'
export const VK_RESPONSE_TYPE = 'code'
export const VK_API_VERSION = '5.73'

export const PUBKEY =
  `-----BEGIN PUBLIC KEY-----
        MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAnBGD/N0TJarpVzRU6lox
        rJmkP6tNJxEbFGVUYWm06FeVzPDMlfjTD3POWfR/9bYwIvFKlhANidEOJf/gbCAJ
        Rl2bsAC4Qg+qq7EJVrJ4K0IjFRxthkb63JLtt3CGVPRUQyvfvnonI3pTVuvibxPd
        hQ4lQO14zWZ8mqFAgCw5AW4xpL8wgdVyHUvXhR2JcvYbEQVZTfBBeLilqW3rTWC+
        DXi4/aYcAQEvAx7s81ybXhfBMYv4UXZF4V2MgzVRBuKWS/PTwA9LuTKcpmPPsGvo
        ki9DAvNMZ7N0pY/YDJHj8f+Co/uilsBkGI6PFy+W0klYABQA4j4p65R1EsUBrm2E
        DwIDAQAB
        -----END PUBLIC KEY-----`
