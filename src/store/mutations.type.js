export const PURGE_AUTH = 'logOut'
export const SET_AUTH = 'setUser'
export const SET_ERROR = 'setError'

export const SET_ARCHIVE_POST_CATEGORY = 'setArchivePostsCategory'
export const ARCHIVE_POSTS_FETCH_START = 'archivePostsFetchStart'
export const ARCHIVE_POSTS_FETCH_END = 'archivePostsFetchEnd'

export const POST_FETCH_START = 'postFetchStart'
export const POST_FETCH_END = 'postFetchEnd'
