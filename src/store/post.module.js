import {GET_POST, GET_POSTS_IS_LOADING} from './getters.type'
import {FETCH_POST} from './actions.type'
import {POST_FETCH_START, POST_FETCH_END} from './mutations.type'
import ApiService from '../common/api.service'

const state = {
  post: {},
  isLoading: false
}

const getters = {
  [GET_POST] (state) {
    return state.post
  },
  [GET_POSTS_IS_LOADING] (state) {
    return state.isLoading
  }
}

const actions = {
  [FETCH_POST] ({commit}, {id, category}) {
    commit(POST_FETCH_START)
    if (category === 'Gods') {
      ApiService.fetchGod(id)
        .then(({data}) => {
          commit(POST_FETCH_END, data)
        })
        .catch((error) => {
          throw new Error(error)
        })
    }
  }
}

const mutations = {
  [POST_FETCH_START] (state) {
    state.isLoading = true
  },
  [POST_FETCH_END] (state, post) {
    state.isLoading = false
    state.post = post
  }
}

export default {
  state,
  actions,
  mutations,
  getters
}
