import Vue from 'vue'
import Vuex from 'vuex'
import auth from './auth.module'
import archive from './archive.module'
import post from './post.module'
import mutations from './mutations'
import actions from './actions'

Vue.use(Vuex)

const state = {
  user: {
    isAuthenticated: undefined,
    id: undefined,
    username: undefined,
    accessLevel: undefined
  },
  error: undefined
}

export default new Vuex.Store({
  state,
  mutations,
  actions,
  modules: {
    auth, archive, post
  }
})
