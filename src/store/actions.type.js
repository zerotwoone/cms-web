export const LOGIN = 'login'
export const SIGNUP = 'signup'
export const LOGOUT = 'logout'
export const CHECK_AUTH = 'checkAuth'

export const FETCH_ARCHIVE_POSTS = 'fetchArchivePosts'

export const FETCH_POST = 'fetchPost'
