import {GET_ARCHIVE_POSTS, GET_ARCHIVE_POSTS_IS_LOADING, GET_ARCHIVE_POSTS_CATEGORY} from './getters.type'
import {FETCH_ARCHIVE_POSTS} from './actions.type'
import {ARCHIVE_POSTS_FETCH_START, ARCHIVE_POSTS_FETCH_END, SET_ARCHIVE_POST_CATEGORY} from './mutations.type'
import ApiService from '../common/api.service'

const state = {
  posts: [],
  category: 'Gods',
  isLoading: false
}

const getters = {
  [GET_ARCHIVE_POSTS] (state) {
    return state.posts
  },
  [GET_ARCHIVE_POSTS_IS_LOADING] (state) {
    return state.isLoading
  },
  [GET_ARCHIVE_POSTS_CATEGORY] (state) {
    return state.category
  }
}

const actions = {
  [FETCH_ARCHIVE_POSTS] ({commit}, category) {
    commit(ARCHIVE_POSTS_FETCH_START)
    commit(SET_ARCHIVE_POST_CATEGORY, category)
    if (category === 'Gods') {
      ApiService.fetchGods()
        .then(({data}) => {
          data = data.map(function (post) {
            return {header: post.name, body: post.info, id: post.id}
          })
          commit(ARCHIVE_POSTS_FETCH_END, data)
        })
        .catch((error) => {
          throw new Error(error)
        })
    } else if (category === 'Religions') {
      ApiService.fetchReligions()
        .then(({data}) => {
          data = data.map(function (post) {
            return {header: post.title, body: post.info, id: post.id}
          })
          commit(ARCHIVE_POSTS_FETCH_END, data)
        })
        .catch((error) => {
          throw new Error(error)
        })
    } else if (category === 'Artifacts') {
      ApiService.fetchArtifacts()
        .then(({data}) => {
          data = data.map(function (post) {
            return {header: post.title, body: post.info, id: post.id}
          })
          commit(ARCHIVE_POSTS_FETCH_END, data)
        })
        .catch((error) => {
          throw new Error(error)
        })
    } else if (category === 'Peoples') {
      ApiService.fetchPeople()
        .then(({data}) => {
          data = data.map(function (post) {
            return {header: post.name, body: post.info, id: post.id}
          })
          commit(ARCHIVE_POSTS_FETCH_END, data)
        })
        .catch((error) => {
          throw new Error(error)
        })
    } else if (category === 'Houses') {
      ApiService.fetchHouses()
        .then(({data}) => {
          data = data.map(function (post) {
            return {header: post.title, body: post.info, id: post.id}
          })
          commit(ARCHIVE_POSTS_FETCH_END, data)
        })
        .catch((error) => {
          throw new Error(error)
        })
    } else if (category === 'Geolocations') {
      ApiService.fetchGeolocations()
        .then(({data}) => {
          data = data.map(function (post) {
            return {header: post.title, body: post.info, id: post.id}
          })
          commit(ARCHIVE_POSTS_FETCH_END, data)
        })
        .catch((error) => {
          throw new Error(error)
        })
    } else if (category === 'Organisations') {
      ApiService.fetchOrganisations()
        .then(({data}) => {
          data = data.map(function (post) {
            return {header: post.title, body: post.info, id: post.id}
          })
          commit(ARCHIVE_POSTS_FETCH_END, data)
        })
        .catch((error) => {
          throw new Error(error)
        })
    } else if (category === 'Events') {
      ApiService.fetchEvents()
        .then(({data}) => {
          data = data.map(function (post) {
            return {header: post.title, body: post.info, id: post.id}
          })
          commit(ARCHIVE_POSTS_FETCH_END, data)
        })
        .catch((error) => {
          throw new Error(error)
        })
    }
  }
}

const mutations = {
  [ARCHIVE_POSTS_FETCH_START] (state) {
    state.isLoading = true
  },
  [SET_ARCHIVE_POST_CATEGORY] (state, category) {
    state.category = category
  },
  [ARCHIVE_POSTS_FETCH_END] (state, posts) {
    state.isLoading = false
    state.posts = posts.map(function (post) {
      if (post.body != null) {
        if (post.body.length > 60) {
          post.body = post.body.substring(0, 60) + ' ...'
        }
      }
      if (post.header != null) {
        if (post.header.length > 19) {
          post.header = post.header.substring(0, 19) + '...'
        }
      }
      return post
    })
  }
}

export default {
  state,
  actions,
  mutations,
  getters
}
