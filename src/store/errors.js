export const AUTH_CHECK_ERROR = 'Error checking authentication. Try reloading page'
export const SERVER_NOT_RESPONDING = 'Error: server is not responding'
export const ERROR_SENDING_REQUEST = 'Couldn\'t send request. Try reloading page'
