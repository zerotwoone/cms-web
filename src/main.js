// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from '@/router'
import store from '@/store'
import {CHECK_AUTH} from '@/store/actions.type'
import ApiService from '@/common/api.service'
import 'bootstrap'
import 'bootstrap/dist/css/bootstrap.min.css'
import vueTopprogress from 'vue-top-progress'

Vue.config.productionTip = false

Vue.use(vueTopprogress)

ApiService.init()

// Ensure we checked auth before each page load.
router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.requiresAuth)) {
    store.dispatch(CHECK_AUTH)
    .then((loggedIn) => loggedIn ? next() : next('/'))
    .catch(() => next(false))
  } else {
    next()
  }
})

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  template: '<App/>',
  components: {App}
})
